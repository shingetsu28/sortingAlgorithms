import threading
import random
import time
from concurrent.futures import ThreadPoolExecutor


def bubbleSort(arr):
    n = len(arr)

    # Traverse through all array elements
    for i in range(n):
        swapped = False

        # Last i elements are already in place
        for j in range(0, n - i - 1):

            # Traverse the array from 0 to n-i-1
            # Swap if the element found is greater
            # than the next element
            if arr[j] > arr[j + 1]:
                arr[j], arr[j + 1] = arr[j + 1], arr[j]
                swapped = True
        if (swapped == False):
            break


def mergeSort(arr):
    if len(arr) > 1:

        # Finding the mid of the array
        mid = len(arr) // 2

        # Dividing the array elements
        L = arr[:mid]

        # Into 2 halves
        R = arr[mid:]

        # Sorting the first half
        mergeSort(L)

        # Sorting the second half
        mergeSort(R)

        i = j = k = 0

        # Copy data to temp arrays L[] and R[]
        while i < len(L) and j < len(R):
            if L[i] <= R[j]:
                arr[k] = L[i]
                i += 1
            else:
                arr[k] = R[j]
                j += 1
            k += 1

        # Checking if any element was left
        while i < len(L):
            arr[k] = L[i]
            i += 1
            k += 1

        while j < len(R):
            arr[k] = R[j]
            j += 1
            k += 1


def insertionSort(arr):
    # Traverse through 1 to len(arr)
    for i in range(1, len(arr)):

        key = arr[i]

        # Move elements of arr[0..i-1], that are
        # greater than key, to one position ahead
        # of their current position
        j = i - 1
        while j >= 0 and key < arr[j]:
            arr[j + 1] = arr[j]
            j -= 1
        arr[j + 1] = key


def selectionSort(arr):
    for i in range(len(arr)):

        min_idx = i
        for j in range(i + 1, len(arr)):
            if arr[min_idx] > arr[j]:
                min_idx = j

        arr[i], arr[min_idx] = arr[min_idx], arr[i]


def printList(arr):
    for i in range(len(arr)):
        print(arr[i], end=" ")
    print()


def cocktailSort(a):
    n = len(a)
    swapped = True
    start = 0
    end = n - 1
    while (swapped == True):
        swapped = False
        for i in range(start, end):
            if (a[i] > a[i + 1]):
                a[i], a[i + 1] = a[i + 1], a[i]
                swapped = True

        if (swapped == False):
            break

        swapped = False

        end = end - 1

        for i in range(end - 1, start - 1, -1):
            if (a[i] > a[i + 1]):
                a[i], a[i + 1] = a[i + 1], a[i]
                swapped = True

        start = start + 1


class QuickSortMultiThreading:
    def __init__(self, start, end, arr):
        self.start = start
        self.end = end
        self.arr = arr

    # Finding random pivoted and partition
    # array on a pivot.
    # There are many different
    # partitioning algorithms.
    # @param start
    # @param end
    # @param arr
    # @return
    def partition(self, start, end, arr):
        i = start
        j = end

        # Decide random pivot
        pivoted = random.randint(i, j)

        # Swap the pivoted with end
        # element of array
        t = arr[j]
        arr[j] = arr[pivoted]
        arr[pivoted] = t
        j -= 1

        # Start partitioning
        while i <= j:
            if arr[i] <= arr[end]:
                i += 1
                continue
            if arr[j] >= arr[end]:
                j -= 1
                continue
            t = arr[j]
            arr[j] = arr[i]
            arr[i] = t
            j -= 1
            i += 1

        # Swap pivoted to its
        # correct position
        t = arr[j + 1]
        arr[j + 1] = arr[end]
        arr[end] = t
        return j + 1

    # Function to implement
    # QuickSort method
    def __call__(self):

        # Base case
        if self.start >= self.end:
            return

        # Find partition
        p = self.partition(self.start, self.end, self.arr)

        # Divide array
        left = QuickSortMultiThreading(self.start, p - 1, self.arr)
        right = QuickSortMultiThreading(p + 1, self.end, self.arr)

        # Left subproblem as separate thread
        with ThreadPoolExecutor(max_workers=2) as executor:
            futures = [executor.submit(left), executor.submit(right)]
            for future in futures:
                future.result()


THREAD_MAX = 4
MAX = 50
part = 0


def merge(low, mid, high):
    left = a[low:mid + 1]
    right = a[mid + 1:high + 1]

    # n1 is size of left part and n2 is size
    # of right part
    n1 = len(left)
    n2 = len(right)
    i = j = 0
    k = low

    # merge left and right in ascending order
    while i < n1 and j < n2:
        if left[i] <= right[j]:
            a[k] = left[i]
            i += 1
        else:
            a[k] = right[j]
            j += 1
        k += 1

    while i < n1:
        a[k] = left[i]
        i += 1
        k += 1

    while j < n2:
        a[k] = right[j]
        j += 1
        k += 1


def merge_sort(low, high):
    if low < high:
        # calculating mid point of array
        mid = low + (high - low) // 2

        merge_sort(low, mid)
        merge_sort(mid + 1, high)

        # merging the two halves
        merge(low, mid, high)


def merge_sort_threaded():
    global part

    # creating 4 threads
    for i in range(THREAD_MAX):
        t = threading.Thread(target=merge_sort, args=(part * (MAX // 4), (part + 1) * (MAX // 4) - 1))
        part += 1
        t.start()

    # joining all 4 threads
    for i in range(THREAD_MAX):
        t.join()

    # merging the final 4 parts
    merge(0, (MAX // 2 - 1) // 2, MAX // 2 - 1)
    merge(MAX // 2, MAX // 2 + (MAX - 1 - MAX // 2) // 2, MAX - 1)
    merge(0, (MAX - 1) // 2, MAX - 1)


def merge_sort_threaded_partial():
    global part

    # creating 4 threads
    for i in range(THREAD_MAX):
        t = threading.Thread(target=merge_sort, args=(part * (MAX // 4), (part + 1) * (MAX // 4) - 1))
        part += 1
        t.start()

    # joining all 4 threads
    for i in range(THREAD_MAX):
        t.join()

    # merging the final 4 parts
    merge(0, (MAX // 2 - 1) // 2, MAX // 2 - 1)
    merge(MAX // 2, MAX // 2 + (MAX - 1 - MAX // 2) // 2, MAX - 1)
    merge(0, (MAX - 1) // 2, MAX - 1)


if __name__ == '__main__':

    MAX = 50
    c = [0] * MAX

    # random array yapan bir kod. 50 item boyutunda array yapiyor
    # c arrayi asil arrayimiz. bunun iki sub arrayi olarak a ve b kullandik.
    # a icin merge, b icin quicksort kullaniyoruz.
    # threadler baglandiktan sonra geri birlesip son siralama olusuyor
    for i in range(MAX):
        c[i] = random.randint(0, 1000)



    a = [0] * 25

    for q in range (0,25,1):
        a[q] = c[q]

    d = [0] * 25
    for x in range (0,25,1):
        d[x] = c[x+25]

    print("Ana listemiz")
    printList(c)


    t1 = time.perf_counter()
    merge_sort_threaded()
    t11 = time.perf_counter()

    t2 = time.perf_counter()
    QuickSortMultiThreading(0,24,d)()
    t22 = time.perf_counter()


    printList(a)
    print(f"subarray a {t11-t1} saniyede duzenlendi")
    printList(d)
    print(f"subarray d {t22-t2} saniyede duzenlendi")
    bubbleSort(c)
    printList(c)
    print("listenin son hali")